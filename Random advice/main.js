const input = document.querySelector('input');
const h1 = document.querySelector('h1');
const arr = [];

const addOption = (e) => {
    e.preventDefault();
    const text = input.value;
    arr.push(text);
    alert(`Dodałeś ${text}`)
    input.value = '';
}

const removeAllOption = (e) => {
    e.preventDefault();
    arr.length = 0;
}

const showAdvice = () => {
    if (arr.length !== 0) {
        let random = Math.floor(Math.random() * arr.length);
        console.log(arr[random]);
        h1.textContent = `Rada to: ${arr[random]}`;
    } else {
        h1.textContent = 'Nie mam dla Ciebie rady.'
    }

}

const showOptions = () => {
    alert(arr.join("Mozliwe opcje to: --- --- "))
}

document.querySelector('.add').addEventListener('click', addOption);
document.querySelector('.clean').addEventListener('click', removeAllOption);
document.querySelector('.showAdvice').addEventListener('click', showAdvice);
document.querySelector('.showOptions').addEventListener('click', showOptions);