const btn = document.querySelector('button');
let number = 1;
const addElement = () => {
    const newElement = document.createElement('div');
    newElement.textContent = number;

    if ((number % 5) == 0) {
        newElement.classList.add('circle');
    }

    document.body.appendChild(newElement);
    number++;
};



btn.addEventListener("click", addElement);