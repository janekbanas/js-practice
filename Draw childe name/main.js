const btn = document.querySelector('button');
const div = document.querySelector('div');
const names = ['Janek', 'Michał', 'Piotr', 'Rafał', 'Władek', 'Wojtek', 'Mikołaj'];

const nameGenerator = () => {
    let random = Math.floor(Math.random() * names.length);
    console.log(names[random]);
    div.textContent = `Imię Twojego dziecka to ${names[random]}`
};

btn.addEventListener("click", nameGenerator);