const box = document.querySelector('div');
let boxX = 150;
let boxY = 50;
box.style.left = boxX + 'px';
box.style.top = `${boxY}px`;

let drawActive = false;

let insertBoxX;
let insertBoxY;

box.addEventListener('mousedown', (e) => {
    box.style.backgroundColor = 'red';
    drawActive = true;

    insertBoxX = e.offsetX;
    insertBoxY = e.offsetY;

});
box.addEventListener('mousemove', (e) => {
    if (drawActive) {

        boxX = e.clientX - insertBoxX;
        boxY = e.clientY - insertBoxY;
        box.style.left = `${boxX}px`;
        box.style.top = `${boxY}px`;
    }
});
box.addEventListener('mouseup', () => {
    box.style.backgroundColor = 'black';
    drawActive = false;

});