let size = 10;
let orderElement = 1;


const init = () => {
    const btn = document.createElement('button');
    document.body.appendChild(btn).textContent = 'KLIK';
    const list = document.createElement('ul');
    document.body.appendChild(list);
    btn.addEventListener('click', creatLiElement);
}

const creatLiElement = () => {
    for (let i = 0; i < 10; i++) {
        const element = document.createElement('li');
        element.textContent = `Element numer ${orderElement++}`;
        element.style.fontSize = `${size++}px`
        document.querySelector('ul').appendChild(element);
    }
}

init();