document.addEventListener('click', (e) => {
    let x = e.clientX;
    let y = e.clientY;
    const red = `rgb(255, 0, 0)`;
    const green = `rgb(0, 255, 0)`;
    const blue = `rgb(0, 0, 255)`;

    const getColor = (e) => {
        if (x % 2 === 0) {
            if (y % 2 === 0) {
                return "red"
            } else {
                return "green"
            }
        } else {
            if (y % 2 !== 0) {
                return "blue"
            } else {
                return "green"
            }
        }
    }

    document.body.style.backgroundColor = getColor(event);
});